# Sugegsted Reading 

## From SoftPrayog
* https://www.softprayog.in/programming/program-process-threads
* https://www.softprayog.in/tutorials/ps-command-usage-examples-in-linux
* https://www.softprayog.in/tutorials/top-command-in-linux
* https://www.softprayog.in/programming/creating-processes-with-fork-and-exec-in-linux
* https://www.softprayog.in/programming/signals-in-linux
* https://www.softprayog.in/programming/posix-threads-programming-in-c
* https://www.softprayog.in/programming/posix-threads-synchronization-in-c
* https://www.softprayog.in/programming/semaphore-basics
* https://www.softprayog.in/programming/posix-semaphores
* https://www.softprayog.in/programming/system-v-semaphores
* https://www.softprayog.in/programming/interprocess-communication-using-posix-message-queues-in-linux
* https://www.softprayog.in/programming/interprocess-communication-using-posix-shared-memory-in-linux
* https://www.softprayog.in/programming/interprocess-communication-using-system-v-message-queues-in-linux
* https://www.softprayog.in/programming/interprocess-communication-using-system-v-shared-memory-in-linux
* https://www.softprayog.in/programming/interprocess-communication-using-fifos-in-linux
* https://www.softprayog.in/programming/interprocess-communication-using-pipes-in-linux

## Virtual Memory
* https://www.youtube.com/playlist?list=PLiwt1iVUib9s2Uo5BeYmwkDFUh70fJPxX 
